## zfs
## 1.  

[root@server ~]# zpool create zfsCompress mirror /dev/sdb /dev/sdc
[root@server ~]# zfs create zfsCompress/gzip
[root@server ~]# zfs create zfsCompress/zle
[root@server ~]# zfs create zfsCompress/lzjb
[root@server ~]# zfs create zfsCompress/lz4
[root@server ~]# zfs compression=gzip zfsCompress/gzip
[root@server ~]# zfs compression=zle zfsCompress/zle
[root@server ~]# zfs compression=lzjb zfsCompress/lzjb
[root@server ~]# zfs compression=lz4 zfsCompress/lz4
[root@server ~]# zfs get compression
NAME              PROPERTY     VALUE     SOURCE
zfsCompress       compression  off       default
zfsCompress/gzip  compression  gzip      local
zfsCompress/lz4   compression  lz4       local
zfsCompress/lzjb  compression  lzjb      local
zfsCompress/zle   compression  zle       local
[root@server ~]# cd /zfsCompress/
[root@server zfsCompress]# ls
gzip  lz4  lzjb  zle
[root@server zfsCompress]# wget -O War_and_Peace.txt https://www.gutenberg.org/files/2600/2600-0.txt
--2021-10-05 21:32:34--  https://www.gutenberg.org/files/2600/2600-0.txt
Resolving www.gutenberg.org (www.gutenberg.org)... 152.19.134.47
Connecting to www.gutenberg.org (www.gutenberg.org)|152.19.134.47|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 3359408 (3.2M) [text/plain]
Saving to: 'War_and_Peace.txt'

War_and_Peace.txt         100%[===================================>]   3.20M  2.21MB/s    in 1.4s

2021-10-05 21:32:36 (2.21 MB/s) - 'War_and_Peace.txt' saved [3359408/3359408]

[root@server zfsCompress]# ls
War_and_Peace.txt  gzip  lz4  lzjb  zle
[root@server zfsCompress]# cp ./War_and_Peace.txt /zfsCompress/gzip/
[root@server zfsCompress]# cp ./War_and_Peace.txt /zfsCompress/lz4
[root@server zfsCompress]# cp ./War_and_Peace.txt /zfsCompress/lzjb
[root@server zfsCompress]# cp ./War_and_Peace.txt /zfsCompress/zle
[root@server zfsCompress]# zfs list
NAME               USED  AVAIL     REFER  MOUNTPOINT
zfsCompress       12.3M   820M     3.28M  /zfsCompress
zfsCompress/gzip  1.24M   820M     1.24M  /zfsCompress/gzip
zfsCompress/lz4   2.02M   820M     2.02M  /zfsCompress/lz4
zfsCompress/lzjb  2.41M   820M     2.41M  /zfsCompress/lzjb
zfsCompress/zle   3.23M   820M     3.23M  /zfsCompress/zle

#Лучше алгоритм сжатия - gzip


## 2.



[root@server ~]# wget "https://drive.google.com/open?id=1KRBNW33QWqbvbVHa3hLJivOAt60yukkg" -O zfs_task1.tar.gz
--2021-10-05 21:59:37--  https://drive.google.com/open?id=1KRBNW33QWqbvbVHa3hLJivOAt60yukkg
Resolving drive.google.com (drive.google.com)... 64.233.161.194
Connecting to drive.google.com (drive.google.com)|64.233.161.194|:443... connected.
HTTP request sent, awaiting response... 307 Temporary Redirect
Location: https://drive.google.com/file/d/1KRBNW33QWqbvbVHa3hLJivOAt60yukkg/view?usp=drive_open [following]
--2021-10-05 21:59:37--  https://drive.google.com/file/d/1KRBNW33QWqbvbVHa3hLJivOAt60yukkg/view?usp=drive_open
Reusing existing connection to drive.google.com:443.
HTTP request sent, awaiting response... 200 OK
Length: unspecified [text/html]
Saving to: 'zfs_task1.tar.gz'

zfs_task1.tar.gz              [ <=>                                ]  64.20K  --.-KB/s    in 0.03s

2021-10-05 21:59:37 (1.97 MB/s) - 'zfs_task1.tar.gz' saved [65740]

[root@server ~]# tar xvzf zfs_task1.tar.gz

gzip: stdin: not in gzip format
tar: Child returned status 1
tar: Error is not recoverable: exiting now
[root@server ~]# tar -xvf zfs_task1.tar.gz

gzip: stdin: not in gzip format
tar: Child returned status 1
tar: Error is not recoverable: exiting now
[root@server ~]# tar -xvf zfs_task1.tar.gz

[root@server ~]# ls
anaconda-ks.cfg  original-ks.cfg  zfs_task1.tar.gz
[root@server ~]# wget "https://drive.google.com/u/0/uc?id=1KRBNW33QWqbvbVHa3hLJivOAt60yukkg&export=download" -O zfs_task2.tar.gz
--2021-10-05 22:10:37--  https://drive.google.com/u/0/uc?id=1KRBNW33QWqbvbVHa3hLJivOAt60yukkg&export=download
Resolving drive.google.com (drive.google.com)... 64.233.161.194
Connecting to drive.google.com (drive.google.com)|64.233.161.194|:443... connected.
HTTP request sent, awaiting response... 302 Moved Temporarily
Location: https://doc-0c-bo-docs.googleusercontent.com/docs/securesc/ha0ro937gcuc7l7deffksulhg5h7mbp1/fh5kd9suh0sadgo6b50btqcdmhl0b83g/1633471800000/16189157874053420687/*/1KRBNW33QWqbvbVHa3hLJivOAt60yukkg?e=download [following]
Warning: wildcards not supported in HTTP.
--2021-10-05 22:10:42--  https://doc-0c-bo-docs.googleusercontent.com/docs/securesc/ha0ro937gcuc7l7deffksulhg5h7mbp1/fh5kd9suh0sadgo6b50btqcdmhl0b83g/1633471800000/16189157874053420687/*/1KRBNW33QWqbvbVHa3hLJivOAt60yukkg?e=download
Resolving doc-0c-bo-docs.googleusercontent.com (doc-0c-bo-docs.googleusercontent.com)... 64.233.161.132
Connecting to doc-0c-bo-docs.googleusercontent.com (doc-0c-bo-docs.googleusercontent.com)|64.233.161.132|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: unspecified [application/x-gzip]
Saving to: 'zfs_task2.tar.gz'

zfs_task2.tar.gz              [     <=>                            ]   6.94M  7.24MB/s    in 1.0s

2021-10-05 22:10:43 (7.24 MB/s) - 'zfs_task2.tar.gz' saved [7275140]

[root@server ~]# tar -xf zfs_task2.tar.gz
[root@server ~]# ls
anaconda-ks.cfg  original-ks.cfg  zfs_task1.tar.gz  zfs_task2.tar.gz  zpoolexport
[root@server ~]# cd zpoolexport/
[root@server zpoolexport]# ls
filea  fileb
[root@server zpoolexport]# cd
[root@server ~]# zpool status
  pool: zfsCompress
 state: ONLINE
  scan: none requested
config:

	NAME         STATE     READ WRITE CKSUM
	zfsCompress  ONLINE       0     0     0
	  mirror-0   ONLINE       0     0     0
	    sdb      ONLINE       0     0     0
	    sdc      ONLINE       0     0     0

errors: No known data errors
[root@server ~]# zpool import -d ${PWD}/zpoolexport/
   pool: otus
     id: 6554193320433390805
  state: ONLINE
 action: The pool can be imported using its name or numeric identifier.
 config:

	otus                         ONLINE
	  mirror-0                   ONLINE
	    /root/zpoolexport/filea  ONLINE
	    /root/zpoolexport/fileb  ONLINE
[root@server ~]# zpool list
NAME          SIZE  ALLOC   FREE  CKPOINT  EXPANDSZ   FRAG    CAP  DEDUP    HEALTH  ALTROOT
zfsCompress   960M  12.3M   948M        -         -     0%     1%  1.00x    ONLINE  -
[root@server ~]# zpool status otus
cannot open 'otus': no such pool
[root@server ~]# zpool import -d ${PWD}/zpoolexport/ otus
[root@server ~]# zpool status otus
  pool: otus
 state: ONLINE
  scan: none requested
config:

	NAME                         STATE     READ WRITE CKSUM
	otus                         ONLINE       0     0     0
	  mirror-0                   ONLINE       0     0     0
	    /root/zpoolexport/filea  ONLINE       0     0     0
	    /root/zpoolexport/fileb  ONLINE       0     0     0

errors: No known data errors
[root@server ~]# zpool list
NAME          SIZE  ALLOC   FREE  CKPOINT  EXPANDSZ   FRAG    CAP  DEDUP    HEALTH  ALTROOT
otus          480M  2.09M   478M        -         -     0%     0%  1.00x    ONLINE  -
zfsCompress   960M  12.3M   948M        -         -     0%     1%  1.00x    ONLINE  -
[root@server ~]# zfs get compression otus
NAME  PROPERTY     VALUE     SOURCE
otus  compression  zle       local
[root@server ~]# zfs get checksum otus
NAME  PROPERTY  VALUE      SOURCE
otus  checksum  sha256     local
[root@server ~]# zfs get recordsize otus
NAME  PROPERTY    VALUE    SOURCE
otus  recordsize  128K     local

#Размер хранилища 480M
#Тип pool - mirror
#recordsize - 128K
#Тип сжатия - zle
#Контрольная сумма - sha256



# 3.




[root@server ~]# wget "https://drive.google.com/u/0/uc?id=1gH8gCL9y7Nd5Ti3IRmplZPF1XjzxeRAG&export=download" -O otus_task2.file
--2021-10-05 22:28:47--  https://drive.google.com/u/0/uc?id=1gH8gCL9y7Nd5Ti3IRmplZPF1XjzxeRAG&export=download
Resolving drive.google.com (drive.google.com)... 64.233.161.194
Connecting to drive.google.com (drive.google.com)|64.233.161.194|:443... connected.
HTTP request sent, awaiting response... 302 Moved Temporarily
Location: https://doc-00-bo-docs.googleusercontent.com/docs/securesc/ha0ro937gcuc7l7deffksulhg5h7mbp1/cupnkcmk7ldrdkm4f4cgosgp2190gq6e/1633472925000/16189157874053420687/*/1gH8gCL9y7Nd5Ti3IRmplZPF1XjzxeRAG?e=download [following]
Warning: wildcards not supported in HTTP.
--2021-10-05 22:28:47--  https://doc-00-bo-docs.googleusercontent.com/docs/securesc/ha0ro937gcuc7l7deffksulhg5h7mbp1/cupnkcmk7ldrdkm4f4cgosgp2190gq6e/1633472925000/16189157874053420687/*/1gH8gCL9y7Nd5Ti3IRmplZPF1XjzxeRAG?e=download
Resolving doc-00-bo-docs.googleusercontent.com (doc-00-bo-docs.googleusercontent.com)... 64.233.161.132
Connecting to doc-00-bo-docs.googleusercontent.com (doc-00-bo-docs.googleusercontent.com)|64.233.161.132|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: unspecified [application/octet-stream]
Saving to: 'otus_task2.file'

otus_task2.file               [    <=>                             ]   5.18M  6.86MB/s    in 0.8s

2021-10-05 22:28:49 (6.86 MB/s) - 'otus_task2.file' saved [5432736]

[root@server ~]# ls
anaconda-ks.cfg  original-ks.cfg  otus_task2.file  zpoolexport
[root@server ~]# zfs receive otus/storage < otus_task2.file
[root@server ~]# zfs list
NAME               USED  AVAIL     REFER  MOUNTPOINT
otus              4.93M   347M       25K  /otus
otus/hometask2    1.88M   347M     1.88M  /otus/hometask2
otus/storage      2.83M   347M     2.83M  /otus/storage
zfsCompress       12.3M   820M     3.28M  /zfsCompress
zfsCompress/gzip  1.24M   820M     1.24M  /zfsCompress/gzip
zfsCompress/lz4   2.02M   820M     2.02M  /zfsCompress/lz4
zfsCompress/lzjb  2.41M   820M     2.41M  /zfsCompress/lzjb
zfsCompress/zle   3.23M   820M     3.23M  /zfsCompress/zle
[root@server ~]# cd /otus/storage/
[root@server storage]# ls
10M.file   Moby_Dick.txt      cinderella.tar    homework4.txt  world.sql
Limbo.txt  War_and_Peace.txt  for_examaple.txt  task1
[root@server storage]# find secret_message
find: 'secret_message': No such file or directory
[root@server storage]# find -name "secret_message"
./task1/file_mess/secret_message
[root@server storage]# vi ./task1/file_mess/secret_message


#Зашифрованное сообщение - https://github.com/sindresorhus/awesome

